export const normalizer = (arrayList: string[], type?: "lower" | "upper") => {
    if(type === "lower") {
       return arrayList.map(item => item.toLowerCase())
    }
    if(type === "upper") {
       return arrayList.map(item => item.toUpperCase())
    }
    return arrayList
  }; 
