import { Produtos } from '../produtos.interface';

export const hasExpiredProduct = (produtos: Produtos[]) => {
    const current = new Date(); 
    return produtos.some((produto) => 
    produto.validade < current) 
}
