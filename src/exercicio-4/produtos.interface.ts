export interface Produtos {
    id: string,
    categoria: string,
    descricao: string,
    promocao?: boolean,
    validade: Date
}
