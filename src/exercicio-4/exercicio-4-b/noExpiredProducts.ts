import { Produtos } from '../produtos.interface';

export const filterNoExpiredProductsByCategory = (produtos: Produtos[], categoria: string ) => {
    const current = new Date();    
    return produtos.filter((produto) => 
    produto.categoria === categoria && produto.validade >= current)
}
