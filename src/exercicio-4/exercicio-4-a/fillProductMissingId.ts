import { Produtos } from '../produtos.interface';

export const fillProductMissingId = (produtos: Produtos[]) => produtos.map(produto => {
    if (!produto.id) {
        return {
            ...produto,
            id: `${Math.floor(Math.random() * 1000)}`
        }
    }
    return produto;
});
