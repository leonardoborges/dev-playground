import { Produtos } from '../produtos.interface';

export const getProductOnSale = (produtos: Produtos[]) => {
    return produtos.find((produto) => produto.promocao)
}
