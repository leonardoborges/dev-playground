import { estados } from "./estados";

export const filterStatesByFirstLetter = (firstLetter: string) => {    
    return estados.UF.filter((item: { nome: string; }) => 
    item.nome.toLowerCase().startsWith(firstLetter.toLowerCase()))
    .map((item) => item.nome)   
};
