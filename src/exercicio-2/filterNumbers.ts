import { numbers } from "./numbers";

export const getOddNumbers = () => {    
    return numbers.filter((number) => number % 2 !== 0);    
};  

export const getEvenNumbers = () => {    
    return numbers.filter((number) => number % 2 === 0);      
};
