import { getProductOnSale } from '../../../../src/exercicio-4//exercicio-4-d/filterByPromotion';
import mock from './mock';

describe('Validate filter that gets the first product on sale method', () => {
    describe('getProductsOnSale', () => {
      it('Should return only one product on sale', () => {
          expect(getProductOnSale(mock.mockProdutos)).toEqual(mock.productOnSale)        
      });

      it('Should not return a product if it is not on sale', () => {
        expect(getProductOnSale(mock.noProductOnSale)).toEqual(undefined)        
      });
    })
})
