import { Produtos } from "../../../../src/exercicio-4/produtos.interface";

const mockProdutos: Produtos[] = [
    {
        id: "1",
        categoria: "alimento",
        descricao: "arroz",
        promocao: false,
        validade: new Date("2021-09-01T23:59:59")
    },
    {
        id: "2",
        categoria: "alimento",
        descricao: "feijão",
        promocao: true,
        validade: new Date("2021-09-01T23:59:59")
    },
    {
        id: "3",
        categoria: "alimento",
        descricao: "café",
        promocao: true,
        validade: new Date("2021-09-01T23:59:59")
    },
    {
        id: "4",
        categoria: "refrigerante",
        descricao: "Coca-Cola",
        promocao: true,
        validade: new Date("2021-09-01T23:59:59")
    }      
];

const productOnSale: Produtos = 
    {
        id: "2",
        categoria: "alimento",
        descricao: "feijão",
        promocao: true,
        validade: new Date("2021-09-01T23:59:59")
    };


const noProductOnSale: Produtos[] = [
    {
        id: "1",
        categoria: "alimento",
        descricao: "arroz",
        promocao: false,
        validade: new Date("2021-09-01T23:59:59")
    },
    {
        id: "1",
        categoria: "alimento",
        descricao: "arroz",
        promocao: false,
        validade: new Date("2021-09-01T23:59:59")
    }
];

export default { mockProdutos, productOnSale, noProductOnSale }
