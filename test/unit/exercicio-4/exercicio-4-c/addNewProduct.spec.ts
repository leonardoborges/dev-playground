import { addNewProduct } from '../../../../src/exercicio-4/exercicio-4-c/addNewProduct';
import mock from './mock';

describe('Validate add new product method', () => {
    describe('addNewProduct', () => {
      it('Should return a new list with a new object', () => {        
        expect(addNewProduct(mock.mockProdutos, mock.newProduct)).toEqual(mock.addProduct)        
      });
    })
})
