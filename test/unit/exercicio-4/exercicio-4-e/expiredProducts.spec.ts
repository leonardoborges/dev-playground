import { hasExpiredProduct } from '../../../../src/exercicio-4/exercicio-4-e/expiredProducts';
import mock from './mock';

describe('Validate expired products method', () => {
    describe('filterNoExpiredProducts', () => {
      it('Should return true when exists an expired product', () => {
        expect(hasExpiredProduct(mock.mockProdutos)).toBeTruthy();
      });

      it('Should return false when not exists an expired product', () => {
        expect(hasExpiredProduct(mock.mockProdutosNaoVencidos)).toBeFalsy();
      });
  });
});
