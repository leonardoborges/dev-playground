import { Produtos } from "../../../../src/exercicio-4/produtos.interface";

const mockProdutos: Produtos[] = [
    {
        id: "1",
        categoria: "alimento",
        descricao: "arroz",
        promocao: true,
        validade: new Date("2021-09-09:23:59:59")
    },
    {
        id: "2",
        categoria: "alimento",
        descricao: "feijão",
        promocao: false,
        validade: new Date("2021-09-01:23:59:59")
    },
    {
        id: "3",
        categoria: "alimento",
        descricao: "oleo",
        promocao: false,
        validade: new Date("2022-10-21:23:59:59")
    },
    {
        id: "4",
        categoria: "refrigerante",
        descricao: "oleo",
        promocao: false,
        validade: new Date("2022-09-19:23:59:59")
    },
    {
        id: "5",
        categoria: "alimento",
        descricao: "café",
        promocao: false,
        validade: new Date("2021-05-05:23:59:59")
    },
]; 

const produtosVencidos: Produtos[] = [
    {
        id: "1",
        categoria: "alimento",
        descricao: "arroz",
        promocao: true,
        validade: new Date("2021-09-09:23:59:59")
    },
    {
        id: "2",
        categoria: "alimento",
        descricao: "feijão",
        promocao: false,
        validade: new Date("2021-09-01:23:59:59")
    },
    {
        id: "5",
        categoria: "alimento",
        descricao: "café",
        promocao: false,
        validade: new Date("2021-05-05:23:59:59")
    }
];

const mockProdutosNaoVencidos: Produtos[] = [
    {
        id: "3",
        categoria: "alimento",
        descricao: "oleo",
        promocao: false,
        validade: new Date("2022-10-21:23:59:59")
    },
    {
        id: "4",
        categoria: "refrigerante",
        descricao: "oleo",
        promocao: false,
        validade: new Date("2023-09-19:23:59:59")
    },
];

export default { mockProdutos, produtosVencidos, mockProdutosNaoVencidos }
