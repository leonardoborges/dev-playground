import { filterNoExpiredProductsByCategory } from '../../../../src/exercicio-4/exercicio-4-b/noExpiredProducts';
import mock from './mock';

describe('Validate filter no expired products method', () => {
  describe('filterNoExpiredProducts', () => {
    it('Should return no expired products from a category passed as a param', () => {
        expect(filterNoExpiredProductsByCategory(mock.mockProdutos, "alimento")).toEqual(mock.alimentosNaoVencidos)
    });
    
    it('Should return no expired products from a category passed as a param', () => {
        expect(filterNoExpiredProductsByCategory(mock.mockProdutos, "refrigerante")).toEqual(mock.refrigerantesNaoVencidos)
    });

    it('Should not return expired products from a category passed as a param ', () => {
      expect(filterNoExpiredProductsByCategory(mock.alimentosVencidos, "alimento")).toEqual([])
    });
    
    it('Should not return expired products from a category passed as a param', () => {
      expect(filterNoExpiredProductsByCategory(mock.refrigerantesVencidos, "refrigerante")).toEqual([])
    }); 
  });
});
