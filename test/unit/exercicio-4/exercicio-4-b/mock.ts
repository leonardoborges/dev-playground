import { Produtos } from "../../../../src/exercicio-4/produtos.interface";

const mockProdutos: Produtos[] = [
    {
        id: "1",
        categoria: "alimento",
        descricao: "arroz",
        promocao: true,
        validade: new Date("2023-09-26T23:59:59")
    },
    {
        id: "2",
        categoria: "alimento",
        descricao: "feijão",
        promocao: false,
        validade: new Date("2021-09-01T23:59:59")
    },
    {
        id: "3",
        categoria: "alimento",
        descricao: "oleo",
        promocao: false,
        validade: new Date("2022-09-19T23:59:59")
    },
    {
        id: "4",
        categoria: "refrigerante",
        descricao: "Coca-Cola",
        promocao: false,
        validade: new Date("2022-09-19T23:59:59")
    },
    {
        id: "5",
        categoria: "refrigerante",
        descricao: "Guaraná Jesus",
        promocao: false,
        validade: new Date("2022-05-19T23:59:59")
    },
    {
        id: "6",
        categoria: "refrigerante",
        descricao: "Tubaína",
        promocao: false,
        validade: new Date("2020-02-15T23:59:59")
    }
]; 

const alimentosNaoVencidos: Produtos[] = [
    {
        id: "1",
        categoria: "alimento",
        descricao: "arroz",
        promocao: true,
        validade: new Date("2023-09-26T23:59:59")
    },
    {
        id: "3",
        categoria: "alimento",
        descricao: "oleo",
        promocao: false,
        validade: new Date("2022-09-19T23:59:59")
    }
];

const refrigerantesNaoVencidos: Produtos[] = [
    {
        id: "4",
        categoria: "refrigerante",
        descricao: "Coca-Cola",
        promocao: false,
        validade: new Date("2022-09-19T23:59:59")
    },
    {
        id: "5",
        categoria: "refrigerante",
        descricao: "Guaraná Jesus",
        promocao: false,
        validade: new Date("2022-05-19T23:59:59")
    }
];

const alimentosVencidos: Produtos[] = [
    {
        id: "2",
        categoria: "alimento",
        descricao: "feijão",
        promocao: false,
        validade: new Date("2021-09-01T23:59:59")
    }
];

const refrigerantesVencidos: Produtos[] = [
    {
        id: "2",
        categoria: "alimento",
        descricao: "feijão",
        promocao: false,
        validade: new Date("2021-02-01T23:59:59")
    }
];

export default { 
    mockProdutos, 
    alimentosNaoVencidos, 
    refrigerantesNaoVencidos,
    alimentosVencidos,
    refrigerantesVencidos
}
