import { Produtos } from "../../../../src/exercicio-4/produtos.interface";

const mockProductsList: Produtos[] = [
    {
        id: "1",
        categoria: "alimento",
        descricao: "arroz",
        promocao: true,
        validade: new Date()
    },
    {
        id: "2",
        categoria: "alimento",
        descricao: "feijão",
        promocao: false,
        validade: new Date()
    },
    {
        id: "3",
        categoria: "alimento",
        descricao: "oleo",
        promocao: false,
        validade: new Date()
    },
    {
        id: "4",
        categoria: "produto de limpeza",
        descricao: "detergente",
        promocao: true,
        validade: new Date()
    },
    {
        id: "5",
        categoria: "produto de limpeza",
        descricao: "sbão em pó",
        promocao: true,
        validade: new Date()
    },
    {
        id: "6",
        categoria: "refrigerante",
        descricao: "Coca-Cola",
        promocao: false,
        validade: new Date()
    },
    {
        id: "7",
        categoria: "refrigerante",
        descricao: "Jesus",
        promocao: true,
        validade: new Date()
    },
    {
        id: undefined,
        categoria: "refrigerante",
        descricao: "Jesus",
        promocao: true,
        validade: new Date()
    },
    {
        id: "8",
        categoria: "refrigerante",
        descricao: "Jesus",
        promocao: true,
        validade: new Date()
    },
    {
        id: "9",
        categoria: "alimento",
        descricao: "café",
        promocao: false,
        validade: new Date()
    },
    {
        id: "10",
        categoria: "alimento",
        descricao: "açúcar",
        promocao: false,
        validade: new Date()
    },
    {
        id: undefined,
        categoria: "alimento",
        descricao: "açúcar",
        promocao: false,
        validade: new Date()
    },
    {
        id: null,
        categoria: "alimento",
        descricao: "farinha",
        promocao: false,
        validade: new Date()
    },
    {
        id: "",
        categoria: "refrigerante",
        descricao: "Guaraná Jesus",
        promocao: false,
        validade: new Date()
    }
];

const mockProductsListFilledId: Produtos[] = [
    {
        id: "1",
        categoria: "alimento",
        descricao: "arroz",
        promocao: true,
        validade: new Date()
    },
    {
        id: "2",
        categoria: "alimento",
        descricao: "feijão",
        promocao: false,
        validade: new Date()
    },
    {
        id: "3",
        categoria: "alimento",
        descricao: "oleo",
        promocao: false,
        validade: new Date()
    },
    {
        id: "4",
        categoria: "produto de limpeza",
        descricao: "detergente",
        promocao: true,
        validade: new Date()
    },
    {
        id: "5",
        categoria: "produto de limpeza",
        descricao: "sbão em pó",
        promocao: true,
        validade: new Date()
    },
    {
        id: "6",
        categoria: "refrigerante",
        descricao: "Coca-Cola",
        promocao: false,
        validade: new Date()
    },
    {
        id: "7",
        categoria: "refrigerante",
        descricao: "Jesus",
        promocao: true,
        validade: new Date()
    },
    {
        id: "123",
        categoria: "refrigerante",
        descricao: "Jesus",
        promocao: true,
        validade: new Date()
    },
    {
        id: "8",
        categoria: "refrigerante",
        descricao: "Jesus",
        promocao: true,
        validade: new Date()
    },
    {
        id: "9",
        categoria: "alimento",
        descricao: "café",
        promocao: false,
        validade: new Date()
    },
    {
        id: "10",
        categoria: "alimento",
        descricao: "açúcar",
        promocao: false,
        validade: new Date()
    },
    {
        id: "456",
        categoria: "alimento",
        descricao: "açúcar",
        promocao: false,
        validade: new Date()
    },
    {
        id: "678",
        categoria: "alimento",
        descricao: "farinha",
        promocao: false,
        validade: new Date()
    },
    {
        id: "91011",
        categoria: "refrigerante",
        descricao: "Guaraná Jesus",
        promocao: false,
        validade: new Date()
    }
];

export default { mockProductsList, mockProductsListFilledId }
