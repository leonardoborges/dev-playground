import { fillProductMissingId } from '../../../../src/exercicio-4/exercicio-4-a/fillProductMissingId';
import { Produtos } from '../../../../src/exercicio-4/produtos.interface';
import mock from './mock';

describe('Validate fill product missing id method', () => {
  describe('fillProductMissingId', () => {
    it('Should fill product id field with random values when id is missing or is invalid', () => {
      jest.spyOn(Math, "random").mockReturnValueOnce(123);
      jest.spyOn(Math, "floor").mockReturnValueOnce(123);

      jest.spyOn(Math, "random").mockReturnValueOnce(456);
      jest.spyOn(Math, "floor").mockReturnValueOnce(456);

      jest.spyOn(Math, "random").mockReturnValueOnce(678);
      jest.spyOn(Math, "floor").mockReturnValueOnce(678);

      jest.spyOn(Math, "random").mockReturnValueOnce(91011);
      jest.spyOn(Math, "floor").mockReturnValueOnce(91011);
    
      expect(fillProductMissingId(mock.mockProductsList)).toEqual(mock.mockProductsListFilledId)
    });

    it('Should not fill product id field when it already has a value', () => {
      const produtos: Produtos[] = [
        {
          id: "1",
          categoria: "alimento",
          descricao: "arroz",
          promocao: true,
          validade: new Date()
      }
      ];

      expect(fillProductMissingId(produtos)).toEqual(produtos)
    });
  });
})
