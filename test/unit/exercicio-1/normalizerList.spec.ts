import { normalizer } from "../../../src/exercicio-1/normalizerList";

describe('normalizer', () => {
    it('Should return string in upper case', () => {        
        const initialString = ["suellen", "neise", "leo"];
        const expectedString = ["SUELLEN", "NEISE", "LEO"];        

        const result = normalizer(initialString, "upper");

        expect(result).toEqual(expectedString);
    });

    it('Should return string in lower case', () => {        
        const initialString = ["SUELLEN", "NEISE", "LEO"];
        const expectedString = ["suellen", "neise", "leo"];

        const result = normalizer(initialString, "lower");

        expect(result).toEqual(expectedString);
    });    

    it('Should return initial string when not normalized', () => {        
        const initialString = ["suellen", "neise", "leo"];
        const expectedString = ["suellen", "neise", "leo"];

        const result = normalizer(initialString);

        expect(result).toEqual(expectedString);
    });
});
