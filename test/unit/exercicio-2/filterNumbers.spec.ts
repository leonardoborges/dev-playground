import { getOddNumbers, getEvenNumbers} from "../../../src/exercicio-2/filterNumbers";

describe('filter numbers', () => {
    describe('getOddNumbers', () => {
        it('Should return odd numbers', () => {
            const oddNumbersList = [
                387, 497, 329,  47, 421, 367, 313, 229,
                53, 451, 177, 123, 185, 341, 495, 309,
                31, 57, 139, 375, 351, 395, 325, 201, 389,
                243, 227, 431
            ]

            expect(getOddNumbers()).toEqual(oddNumbersList)
        });
    });

    describe('getEvenNumbers', () => {
        it('Should return even numbers', () => {
            const evenNumbersList = [
                102, 4, 448, 366, 186, 350, 60, 12, 120, 310,
                42, 322, 176, 298, 66, 168, 398, 354, 382,
                384, 164, 40
            ]

            expect(getEvenNumbers()).toEqual(evenNumbersList)
        });
    });
});
