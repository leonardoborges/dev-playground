import { filterStatesByFirstLetter } from "../../../src/exercicio-3/filterEstados";

describe('filter states by first letter passed as param', () => {
    describe('filterByFirstLetter', () => {
        it('Should return states with first letter P in upperCase', () => {            
            const result = filterStatesByFirstLetter("P");

            expect(result).toEqual(['Pará', 'Paraíba', 'Paraná', 'Pernambuco','Piauí'])
        });
    
        it('Should return states with first letter p in lowerCase', () => {            
            const result = filterStatesByFirstLetter("p");

            expect(result).toEqual(['Pará', 'Paraíba', 'Paraná', 'Pernambuco','Piauí'])
        });

        it('Should not return states that do not have the first letter passed as param ', () => {            
            const result = filterStatesByFirstLetter("Q");

            expect(result).toEqual([])
        });
    });   
});
