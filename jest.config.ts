import type { Config } from '@jest/types';

const config: Config.InitialOptions = {
  roots: ['<rootDir>/test/unit/', '<rootDir>/src/'],
  verbose: true,
  preset: 'ts-jest',
  testEnvironment: 'node',
  collectCoverage: true,
  coverageThreshold: {
    global: {
        statements: 100,
        branches: 100,
        functions: 100,
        lines: 100
    }
},
  coverageDirectory: process.env.JEST_CLOVER_OUTPUT_DIR || './_devops/artifacts/reports',
  reporters: ['default', 'jest-junit'],
  coverageReporters: ['text', 'clover', 'lcov', 'json-summary'],
  collectCoverageFrom: ['<rootDir>/src/**/*.ts'],
  modulePathIgnorePatterns: ['dist', '.stryker-tmp'],
  // moduleFileExtensions: ['ts', 'tsx', 'js', 'json', 'jsx'],
  modulePaths: ['<rootDir>', 'src'],
};

export default config;
